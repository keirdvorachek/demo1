// Keir Dvorachek
/* Demo 1*/


// These are files that the functions below use i.e. _getch comes out of conio.h
#include <iostream>
#include <conio.h>

using namespace std;

int main() 
{
	// std = standard     cout = console output      << = stream indicator
	cout << "Hello \"World!\"\n"; // This is not a string variable.

	int i;
	float f;
	double d;
	bool b;
	char c;

	const double PI = 3.1415926;
	// double const PI = 3.1415926;

	cout << "Enter an integer; \n";
	cin >> i;


	if (i > 100) cout << "That's a big number.\n";
	else cout << "That's not  big number.\n";

	if (i) cout << "I is not zero.\n";

	// Switch case example online.

	for (int i = 0; i <= 10; i++) cout << 1 << "\n";

	cout << "Do you want to do math? (y/n);";
	int x = 1;
	char input;
	//cin >> input;

	while (input == 'y')
	{
		cout << "9 x " << x << " = " << 9 * x << "\n";
		cout << "Again?";
		cin >> input;
		x++;
	}
	

	_getch();
	return 0;
}